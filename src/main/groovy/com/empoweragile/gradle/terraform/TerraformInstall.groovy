package com.empoweragile.gradle.terraform

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.gradle.internal.os.OperatingSystem

import static org.apache.tools.ant.taskdefs.condition.Os.*

class TerraformInstall extends DefaultTask{
    def osInfo = new OsInfo()
    def buildDir = project.buildDir

    def segmentFromArchitecture = [
            "i386": "386",
            "x86": "386",
            "amd64": "amd64"
    ]

    def segmentFromOs() {
        [
                "windows": osInfo.isWindows(),
                "linux"  : osInfo.isLinux(),
                "darwin" : osInfo.isMacOsX()
        ]
    }

    @TaskAction
    void perform() {
        project.logger.debug "Downloading Terraform from $url() ."
    }

    def url(){
        def version = project.extensions.terraform.version
        "https://releases.hashicorp.com/terraform/${version}/terraform_${version}_${os()}_${architecture()}.zip"
    }

    def installDir(){
        new File( "${osInfo.gradleUserHomeDir()}${osInfo.slash()}caches${osInfo.slash()}terraform${osInfo.slash()}${project.extensions.terraform.version}")
    }

    def tempDir(){
        new File("${buildDir}${osInfo.slash()}tmp")
    }

    def architecture(){
        def groovyArchitecture = ["i386", "x86","amd64"].find{
            osInfo.isArchitecture(it)
        }
        segmentFromArchitecture[groovyArchitecture]
    }

    def os(){
        segmentFromOs().find {it.value==true}.key
    }

    def installed(){
        false
    }

    class OsInfo{
        def isWindows(){
            OperatingSystem.current().isWindows()
        }
        def isLinux(){
            OperatingSystem.current().isLinux()
        }
        def isMacOsX(){
            OperatingSystem.current().isMacOsX()
        }

        def isArchitecture(String architecure){
            isArch(architecure)
        }
        def gradleUserHomeDir(){
            project.gradle.gradleUserHomeDir
        }
        def slash(){
            System.getProperty("file.separator")
        }
    }
}


