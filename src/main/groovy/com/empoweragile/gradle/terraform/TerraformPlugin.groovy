package com.empoweragile.gradle.terraform

import org.gradle.api.Plugin
import org.gradle.api.Project

class TerraformPlugin implements Plugin<Project> {
    void apply(Project project){
        project.extensions.create("terraform", TerraformPluginExtension)

        project.task('install', type: TerraformInstall) {
            group = 'com.empoweragile.gradle'
            description = 'Installs Terraform to a temporary location.'
        }
    }

}