import com.empoweragile.gradle.terraform.TerraformPlugin
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class TerraformInstallTest extends Specification {

    def task
    def project
    def slash = System.getProperty("file.separator")
    def setup() {
        project = ProjectBuilder.builder().build()
        project.with {
            version = ''
            apply plugin: TerraformPlugin
        }
        task = project.tasks.install
        project.extensions.findByName("terraform").version = "1.0.0"

    }

    def "should initialize plugin"(){
        given:
        def win64 = new OsInfoMock(arch:"amd64", windows: true)

        when:
        task.osInfo = win64

        then:
        task.architecture() == "amd64"
        task.os() == "windows"
        task.url() == "https://releases.hashicorp.com/terraform/1.0.0/terraform_1.0.0_windows_amd64.zip"
    }

    def "should generate url based on underlying os"(){
        given:
        def mockOsInfo = new OsInfoMock(windows: windows, linux:linux, macOsX: macOsX, arch: arch)
        task.osInfo = mockOsInfo

        expect:
        task.os() == os
        task.architecture() == architecture
        task.url() == url

        where: "os arch"
        windows || linux || macOsX || arch    ||os        ||architecture || url
        true    || false || false  || "amd64" ||"windows" ||"amd64"      ||"https://releases.hashicorp.com/terraform/1.0.0/terraform_1.0.0_windows_amd64.zip"
        true    || false || false  || "i386"  ||"windows" ||"386"        ||"https://releases.hashicorp.com/terraform/1.0.0/terraform_1.0.0_windows_386.zip"
        false   || true  || false  || "amd64" ||"linux"   ||"amd64"      ||"https://releases.hashicorp.com/terraform/1.0.0/terraform_1.0.0_linux_amd64.zip"
        false   || true  || false  || "i386"  ||"linux"   ||"386"        ||"https://releases.hashicorp.com/terraform/1.0.0/terraform_1.0.0_linux_386.zip"
        false   || false || true   || "amd64" ||"darwin"  ||"amd64"      ||"https://releases.hashicorp.com/terraform/1.0.0/terraform_1.0.0_darwin_amd64.zip"
    }

    def "should generate install location based on terraform version"(){
        given:
        def mockOsInfo = new OsInfoMock(slash:"/")

        def expectedInstallDir = new File("gradleUserHome${slash}caches${slash}terraform${slash}1.0.0")

        when:
        task.osInfo = mockOsInfo

        then:
        task.installDir() == expectedInstallDir
    }

    def "should place temporary files in build/tmp"(){
        given:
        task.buildDir = "build"

        when:
        def expectedTempDir = new File("build${slash}tmp")

        then:
        task.tempDir() == expectedTempDir
    }

    def "should check if terraform already installed"(){
        when:
        task.buildDir = "build"

        then:
        task.installed() == false
    }

}

class OsInfoMock{
    def windows
    def linux
    def macOsX
    def arch
    def slash
    def isWindows(){
        windows
    }
    def isLinux(){
        linux
    }
    def isMacOsX(){
        macOsX
    }
    def isArchitecture(architecure){
        this.arch == architecure
    }
    def gradleUserHomeDir(){
        "gradleUserHome"
    }
    def slash(){
        return  slash
    }
}