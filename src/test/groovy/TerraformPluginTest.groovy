import com.empoweragile.gradle.terraform.TerraformInstall
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification
import com.empoweragile.gradle.terraform.TerraformPlugin

class TerraformPluginTest extends Specification {

    def project

    def setup() {
        project = ProjectBuilder.builder().build()
        project.with {
            version = ''
            apply plugin: com.empoweragile.gradle.terraform.TerraformPlugin
        }
    }

    def "should initialize plugin"(){
        given:
        1 == 1

        when:
        1 == 1

        then:
        project.version == ''
        project.tasks.install instanceof TerraformInstall
        project.tasks.install.group == 'com.empoweragile.gradle'
    }

}